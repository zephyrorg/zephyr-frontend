const Prismic = require('prismic-javascript');
const fs = require('fs');

const apiEndpoint = 'https://zephyrgaming.prismic.io/api/v2';
const accessToken = process.env.PRISMIC_ACCESS_TOKEN;

Prismic.getApi(apiEndpoint, {
  accessToken,
}).then(function (api) {
  return Promise.all([
    // Charter
    api.query(Prismic.Predicates.at('document.type', 'charter')),
  ]);
}).then(function (responses) {
  // Charter
  fs.writeFileSync('src/data/json/charter.json', JSON.stringify(responses[0]), {
    encoding: 'utf8',
    flag: 'w'
  });
});
