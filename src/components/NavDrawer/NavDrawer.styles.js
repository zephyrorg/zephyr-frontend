const styles = theme => ({
  root: {},
  drawer: {
    width: 300,
  },
  navButton: {
    backgroundColor: '#7289da',
    '&:hover': {
      backgroundColor: '#495991',
    },
    ...theme.mixins.noSelect(),
  },
  navButtonIcon: {
    marginRight: 10,
    fontSize: 22,
  },
  buttonLink: {
    margin: theme.spacing.unit * 4,
    '&:hover': {
      textDecoration: 'none',
    },
  },
  listItem: {
    ...theme.mixins.noSelect(),
  },
});

export default styles;
