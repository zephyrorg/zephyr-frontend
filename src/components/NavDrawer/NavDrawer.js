import React from 'react';
import { withRouter } from 'react-router-dom';
import { connect } from 'react-redux';
import HomeIcon from '@material-ui/icons/Home';
import AssignmentIcon from '@material-ui/icons/Assignment';
import ExitIcon from '@material-ui/icons/ExitToApp';
import CalendarTodayIcon from '@material-ui/icons/CalendarToday';
import SwipeableDrawer from '@material-ui/core/SwipeableDrawer';
import Divider from '@material-ui/core/Divider';
import List from '@material-ui/core/List';
import ListItem from '@material-ui/core/ListItem';
import ListItemIcon from '@material-ui/core/ListItemIcon';
import ListItemText from '@material-ui/core/ListItemText';
import ListSubheader from '@material-ui/core/ListSubheader';
import { withStyles } from '@material-ui/core/styles';
import styles from './NavDrawer.styles';
import classnames from 'classnames';
import { openNavDrawer, closeNavDrawer } from './NavDrawer.actions';
import Button from '@material-ui/core/Button/Button';
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';
import { withCookies } from 'react-cookie';
import { logoutUser } from '../../utils/api.actions';

@withRouter
@withStyles(styles)
@withCookies
@connect(state => ({
  open: state.mobileDrawerState.isOpen,
  username: state.apiState.username,
}), { openNavDrawer, closeNavDrawer, logoutUser })
class NavDrawer extends React.Component {
  constructor(props) {
    super(props);
    this.items = 0;
  }


  navItem = (label, onClick, Icon = HomeIcon, loggedInState = null) => {
    const { loggedIn } = this.props;
    if (loggedInState === true && !loggedIn) return null;
    this.items += 1;
    return (
      <ListItem key={this.items} button onClick={onClick}>
        <ListItemIcon>
          <Icon />
        </ListItemIcon>
        <ListItemText primary={label} />
      </ListItem>
    );
  };

  navSection = (label, items = [], className = '') => {
    const { classes } = this.props;
    const subheader = <ListSubheader component="div">{label}</ListSubheader>;
    return (
      <div className={classnames(classes.options, className)}>
        <Divider />
        <List component="nav"
              subheader={label === null ? undefined : subheader}>
          {items}
        </List>
      </div>
    );
  };

  authNav = () => {
    const { classes, username } = this.props;
    if (!username) {
      return (
        <a href={`${process.env.REACT_APP_API_ENDPOINT}/discord/oauth`} className={classes.buttonLink}>
          <Button classes={{
            root: classes.navButton
          }}>
            <FontAwesomeIcon icon={['fab', 'discord']} className={classes.navButtonIcon}/> Login via Discord
          </Button>
        </a>
      );
    }
    return null;
  };

  render() {
    const iOS = process.browser && /iPad|iPhone|iPod/.test(navigator.userAgent);
    const { open, classes } = this.props;
    return (
      <SwipeableDrawer
        open={open}
        onClose={this.props.closeNavDrawer}
        onOpen={this.props.openNavDrawer}
        className={classes.root}
        disableBackdropTransition={!iOS}
        disableDiscovery={iOS}
      >
        <div
          className={classes.drawer}
          tabIndex={0}
          role="button"
          onClick={this.props.closeNavDrawer}
          onKeyDown={this.props.closeNavDrawer}
        >
          {this.navSection(null, [
            this.navItem('Home', () => this.props.history.push('/'), HomeIcon),
            this.navItem('Charter', () => this.props.history.push('/charter'), AssignmentIcon),
            this.navItem('Schedule', () => this.props.history.push('/schedule'), CalendarTodayIcon),
            this.navItem('Logout', this.props.logoutUser, ExitIcon, true),
          ])}
          {this.authNav()}
        </div>
      </SwipeableDrawer>
    );
  }

}

export default NavDrawer;