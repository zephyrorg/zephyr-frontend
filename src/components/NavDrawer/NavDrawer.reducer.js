import { OPEN_NAV_DRAWER, CLOSE_NAV_DRAWER } from './NavDrawer.actions';

const INITIAL_STATE = {
  isOpen: false,
};

const reducer = (state = INITIAL_STATE, action) => {
  switch (action.type) {
    case OPEN_NAV_DRAWER:
      return {
        ...state,
        isOpen: true,
      };
    case CLOSE_NAV_DRAWER:
      return {
        ...state,
        isOpen: false,
      };
    default:
      return state;
  }
};

export default reducer;
