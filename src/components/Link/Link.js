import React from 'react';
import { Link as ReactLink } from 'react-router-dom';

class Link extends React.Component {
  static parseTo(to) {
    let parser = document.createElement('a');
    parser.href = to;
    return parser;
  }

  static isInternal(to) {
    // If it's a relative url such as '/path', 'path' and does not contain a protocol we can assume it is internal.
    if(to.indexOf("://")=== -1) return true;
    const toLocation = Link.parseTo(to);
    return window.location.hostname === toLocation.hostname;
  }

  render() {
    const {to, children, ...rest} = this.props;
    const isInternal = Link.isInternal(to);
    if (isInternal) {
      return (<ReactLink to={to} {...rest}>{children}</ReactLink>);
    } else {
      return (<a rel="noopener noreferrer" href={to} target="_blank" {...rest}>{children}</a>);
    }
  }
}

export default Link;
