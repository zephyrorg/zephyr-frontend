import React from 'react';
import Edit from '@material-ui/icons/Edit';
import { withStyles } from '@material-ui/core/styles';
import styles from './CreateAccount.styles';
import Input from '../Forms/Input';
import Grid from '@material-ui/core/Grid';
import Button from '../Forms/Button';

@withStyles(styles)
class CreateAccount extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      values: {},
    };
  }

  handleChange = (name) => (e) => {
    const { value } = e.target;
    this.setState((state) => {
      state.values[name] = value;
      return state;
    });
  };

  handleSubmit = (e) => {
    e.preventDefault();
  };

  render() {
    const { classes } = this.props;

    return (
      <div className={classes.root}>
        <h3 className={classes.h3}>Create Account</h3>
        <div>
          <form onSubmit={this.handleSubmit}>
            <Grid container>
              <Grid item xs={12}>
                <Input label="E-Mail Address" name="email" type="email"
                  description="You will login via your e-mail address. You will need to verify it."
                  onChange={this.handleChange}
                />
              </Grid>
              <Grid item xs={12} md={6}>
                <Input label="Password" name="password" type="password"
                       onChange={this.handleChange}
                       validate={(value) => {
                         switch (value) {
                           default:
                             return 'fdfadf';
                         }
                       }}
                />
              </Grid>
              <Grid item xs={12} md={6}>
                <Input label="Confirm Password" name="confirmPassword" type="password"
                       onChange={this.handleChange}
                />
              </Grid>
              <Grid item xs={12} className={classes.textRight}>
                <Button label="Create" Icon={Edit} type="submit" />
              </Grid>
            </Grid>
          </form>
        </div>
      </div>
    );
  }
}

export default CreateAccount;
