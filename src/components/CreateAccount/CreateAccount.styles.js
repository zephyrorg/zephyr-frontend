const styles = theme => ({
  root: {
    ...theme.mixins.verticalAlign(),
    maxWidth: 600,
    backgroundColor: 'rgba(16,16,16,0.8)',
    borderRadius: 12,
    margin: 'auto',
    padding: theme.spacing.unit * 2,
  },
  h3: {
    fontWeight: 600,
    textAlign: 'center',
  },
  formGroup: {
    composes: [ 'form-group' ],
  },
  textRight: {
    textAlign: 'right',
  },
});

export default styles;