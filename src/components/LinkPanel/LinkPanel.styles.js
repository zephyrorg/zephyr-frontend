const styles = theme => ({
  root: {
    borderRadius: 12,
    [theme.breakpoints.between('xs', 'md')]: {
      height: 400,
    },
    [theme.breakpoints.between('md', 'xl')]: {
      height: 400,
    },
    width: '100%',
    boxShadow: 'inset 0.5px 0.5px 0.5px #9b9b9b, 0px 0px 10px rgba(16,16,16,0.5)',
    position: 'relative',
    '&:hover': {
      cursor: 'pointer',
    },
    '&:before': {
      [theme.breakpoints.between('xs', 'md')]: {
        opacity: 1,
      },
      [theme.breakpoints.between('md', 'xl')]: {
        opacity: 0,
      },
      webkitTransition: 'opacity 0.2s ease-in-out',
      MozTransition: 'opacity 0.2s ease-in-out',
      OTransition: 'opacity 0.2s ease-in-out',
      transition: 'opacity 0.2s ease-in-out',
      backgroundImage: 'linear-gradient(to top, #101010 0%, rgba(183, 158, 112, 0) 100%)',
      [theme.breakpoints.between('xs', 'sm')]: {
        height: 400,
      },
      [theme.breakpoints.between('sm', 'xl')]: {
        height: 400,
      },
      width: '100%',
      content: '""',
      borderRadius: 12,
      position: 'absolute',
      top: 0,
    },
    '&:hover:before': {
      opacity: 1,
    },
  },
  half: {
    height: 200,
    '&:before': {
      height: 200,
    },
  },
  comingSoon: {
    [theme.breakpoints.between('xs', 'md')]: {
      opacity: 1,
    },
    [theme.breakpoints.between('md', 'xl')]: {
      opacity: 0,
    },
    webkitTransition: 'opacity 0.2s ease-in-out',
    MozTransition: 'opacity 0.2s ease-in-out',
    OTransition: 'opacity 0.2s ease-in-out',
    transition: 'opacity 0.2s ease-in-out',
    backgroundImage: 'linear-gradient(to top, #101010 0%, rgba(183, 158, 112, 0) 100%)',
    height: 400,
    width: '100%',
    content: '""',
    borderRadius: 12,
    position: 'absolute',
    top: 0,
    zIndex: 1100,
    color: '#fff',
    fontSize: 30,
    '&:hover': {
      opacity: 1,
    },
  },
  comingSoonText: {
    ...theme.mixins.verticalAlign(),
    textAlign: 'center',
    marginTop: -50,
  },
  content: {
    color: '#fff',
    position: 'absolute',
    bottom: 0,
    left: 0,
    padding: 30,
    paddingBottom: 40,
  },
  title: {
    fontWeight: 700,
    textShadow: '2px 2px 5px rgba(16,16,16,0.8)',
    [theme.breakpoints.between('xs', 'md')]: {
      fontSize: '2rem',
    },
    [theme.breakpoints.between('md', 'xl')]: {
      fontSize: '2.5rem',
    },
  },
  subtitle: {
    [theme.breakpoints.between('xs', 'md')]: {
      fontSize: '1.32rem',
    },
    [theme.breakpoints.between('md', 'xl')]: {
      fontSize: '1.75rem',
    },
  },
  action: {
    [theme.breakpoints.between('xs', 'md')]: {
      display: 'none',
    },
    [theme.breakpoints.between('md', 'xl')]: {
      display: 'block',
    },
    position: 'absolute',
    bottom: 50,
    right: 50,
  }
});

export default styles;
