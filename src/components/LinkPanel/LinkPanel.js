import React from 'react';
import PropTypes from 'prop-types';
import { withStyles } from '@material-ui/core/styles';
import Button from '@material-ui/core/Button';
import styles from './LinkPanel.styles';
import Link from '../Link';
import classnames from 'classnames';

@withStyles(styles)
class LinkPanel extends React.Component {
  getSubtitle = () => {
    const { subtitle, classes } = this.props;
    if (subtitle) {
      return (
        <h3 className={classes.subtitle}>{subtitle}</h3>
      );
    }
    return null;
  };

  getComingSoon = () => {
    const { comingSoon, classes, small } = this.props;

    if (comingSoon) {
      return (
        <div className={classnames({
          [classes.small]: small
        }, classes.comingSoon)}>
          <h2 className={classes.comingSoonText}>Coming Soon</h2>
        </div>
      );
    }
    return null;
  };

  actionButton = () => {
    const { action, classes } = this.props;
    if (action === null) return null;
    return (
      <Button className={classes.action} variant="contained" color="default">{action}</Button>
    );
  };

  render() {
    const { classes, backgroundImage, backgroundPosition, title, to, half, onClick } = this.props;

    const inlineStyles = {
      backgroundImage: `url(${backgroundImage})`,
      backgroundSize: 'cover',
    };

    if (backgroundPosition) {
      inlineStyles.backgroundPosition = backgroundPosition;
    }

    const inner = (
      <div style={inlineStyles} className={classnames(classes.root, {
        [classes.half]: half,
      })}>
        <div className={classes.content}>
          <h1 className={classes.title}>{title}</h1>
          {this.getSubtitle()}
        </div>
        {this.getComingSoon()}
        {this.actionButton()}
      </div>
    );

    if (onClick) {
      return (
        <div onClick={onClick}>
          {inner}
        </div>
      );
    }

    return (
      <Link to={to}>
        {inner}
      </Link>
    );
  }
}

LinkPanel.defaultProps = {
  external: false,
  comingSoon: false,
  action: null,
};

LinkPanel.propTypes = {
  backgroundImage: PropTypes.string.isRequired,
  to: PropTypes.string,
  external: PropTypes.bool,
  comingSoon: PropTypes.bool,
  action: PropTypes.oneOfType([PropTypes.string, PropTypes.object]),
};

export default LinkPanel;
