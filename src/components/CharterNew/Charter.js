import React from 'react';
import styles from './Charter.styles';
import { withStyles } from '@material-ui/core/styles';
import Grid from '@material-ui/core/Grid';
import { Element } from 'react-scroll';
import Prismic from 'prismic-javascript';
import charterJson from '../../data/json/charter';

const {
  REACT_APP_CMS_ENDPOINT,
  REACT_APP_CMS_LIVE,
  REACT_APP_PRISMIC_ACCESS_TOKEN,
} = process.env;

@withStyles(styles)
class Charter extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      coreCharter: null,
      charterSections: null,
    };
  }

  componentDidMount() {
    if (REACT_APP_CMS_LIVE === 'true') {
      const self = this;
      Prismic.getApi(REACT_APP_CMS_ENDPOINT, {
        accessToken: REACT_APP_PRISMIC_ACCESS_TOKEN,
      }).then(function (api) {
        return Promise.all([
          // Charter
          api.query(Prismic.Predicates.at('document.type', 'charter')),
        ]);
      }).then(function (responses) {
        const charter = responses[0].results[0].data;
        const coreCharter = charter.core_charter;
        const charterSections = charter.charter_sections;
        self.setState({
          coreCharter,
          charterSections,
        });
      });
    } else {
      const charter = charterJson.results[0].data;
      const coreCharter = charter.core_charter;
      const charterSections = charter.charter_sections;
      this.setState({
        coreCharter,
        charterSections,
      });
    }
  }

  makeList = (listItems, key) => {
    const { classes } = this.props;
    return (
      <Grid item xs={12} className={classes.paragraphList} key={key}>
        <ol className={classes.orderedList}>
          {listItems.map((item, key) => (<li key={key}><p>{this.parseText(item)}</p></li>))}
        </ol>
      </Grid>
    );
  };

  makeParagraph = (content, key) => {
    const { classes } = this.props;
    return (
      <Grid item xs={12} className={classes.paragraph} key={key}>
        {content}
      </Grid>
    );
  };

  parseText = (text) => {
    const split = text.split('\n');
    if (split.length > 1) {
      return text.split('\n').map((item, key) => (<span key={key}>{item}<br/></span>))
    }
    return text;
  };

  makeSection = (section, key) => {
    const { classes } = this.props;
    return (
      <Grid item xs={12} className={classes.paragraphList} key={key}>
        <Element name={section.charter_section_title[0].text}>
          <h2>{section.charter_section_title[0].text}</h2>
        </Element>
        {this.print(section.charter_section_content)}
      </Grid>
    );
  };

  print = (data) => {
    const items = [];
    data.forEach((item) => {
      if (item.type === 'paragraph') {
        items.push(this.parseText(item.text));
      } else if (item.type === 'o-list-item') {
        const lastItem = items[items.length-1];
        if (Array.isArray(lastItem)) {
          items[items.length-1].push(item.text);
        } else {
          items.push([item.text]);
        }
      }
    });
    return items.map((item, key) => {
      if (Array.isArray(item)) {
        return this.makeList(item, key);
      } else {
        return this.makeParagraph(item, key);
      }
    });
  };

  render() {
    const { classes } = this.props;
    const { coreCharter, charterSections } = this.state;

    if (coreCharter !== null && charterSections !== null) {
      return (
        <Grid container className={classes.root}>
          <Grid item xs={12} className={classes.header}>
            <h1 className={classes.title}>Legion Charter</h1>
          </Grid>
          {this.print(coreCharter)}
          {charterSections.map((section, key) => this.makeSection(section, key))}
        </Grid>
      );
    } else {
      return null;
    }
  }

}

export default Charter;
