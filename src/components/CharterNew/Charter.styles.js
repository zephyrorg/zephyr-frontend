const styles = theme => ({
  root: {
    paddingTop: 10,
  },
  title: {
    margin: 10,
  },
  header: {
    textAlign: 'center',
  },
  paragraph: {
    margin: 20,
  },
  spanLine: {
    display: 'block',
  },
  paragraphList: {
    margin: 10,
    marginLeft: 20,
    marginRight: 50,
  },
  paragraphListHalfLeft: {
    paddingLeft: 50,
    paddingRight: 20,
  },
  paragraphListHalfRight: {
    paddingRight: 50,
    paddingLeft: 20,
  },
  orderedList: {
    '& li': {
      marginBottom: 20,
    },
  },
  links: {
    textAlign: 'center',
  },
  button: {
    ...theme.mixins.noSelect(),
    margin: theme.spacing.unit,
    backgroundColor: '#101010',
    color: '#fff',
    '&:hover': {
      backgroundColor: '#2a2a2a',
    },
  },
});

export default styles;
