const styles = theme => ({
  root: {
    margin: 'auto',
  },
  frame: {
    position: 'relative',
  },
  image: {
    WebkitTransition: 'opacity 1s ease-in-out',
    MozTransition: 'opacity 1s ease-in-out',
    OTransition: 'opacity 1s ease-in-out',
    transition: 'opacity 1s ease-in-out',
    opacity: 0,
    position: 'absolute',
    top: 0,
    left: 0,
    [theme.breakpoints.between('xs', 'md')]: {
      maxWidth: '100% !important',
    },
  },
  top: {
    opacity: 1,
  },
});

export default styles;
