import React from 'react';
import { withStyles } from '@material-ui/core/styles';
import styles from './SlideShow.styles';
import PropTypes from 'prop-types';
import classnames from 'classnames';

@withStyles(styles)
class SlideShow extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      imageKey: 0,
    };
  }

  componentDidMount() {
    const { images } = this.props;
    setInterval(() => {
      this.setState((state) => {
        const currentImageKey = state.imageKey;
        let newImageKey = state.imageKey + 1;
        const isLastImage = currentImageKey === (images.length - 1);
        if (isLastImage) {
          newImageKey = 0;
        }
        return {
          imageKey: newImageKey,
        };
      });
    }, 5000);
  };

  imageSizeStyle = () => {
    const { width, height } = this.props;
    return {
      width, height,
    };
  };

  images = () => {
    const { images, classes } = this.props;
    const { imageKey } = this.state;
    return images.map((image, key) => {
      return (
        <img key={key} style={this.imageSizeStyle()} alt="" className={classnames(classes.image, {
          [classes.top]: key === imageKey,
        })} src={image} />
      );
    });
  };

  render() {
    const { classes } = this.props;
    return (
      <div className={classes.root}>
        <div className={classes.frame}>
          {this.images()}
        </div>
      </div>
    );
  }
}

SlideShow.propTypes = {
  width: PropTypes.number.isRequired,
  height: PropTypes.number.isRequired,
  images: PropTypes.array.isRequired,
};

export default SlideShow;
