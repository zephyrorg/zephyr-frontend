import React from 'react';
import MaterialButton from '@material-ui/core/Button';
import PropTypes from 'prop-types';
import { withStyles } from '@material-ui/core/styles';
import styles from './Button.styles';
import PlayArrow from '@material-ui/icons/PlayArrow';

@withStyles(styles)
class Button extends React.Component {
  render() {
    const { label, classes, Icon, type } = this.props;
    return (
      <MaterialButton
        variant="contained"
        className={classes.root}
        type={type}
        color="primary">
        <Icon className={classes.extendedIcon} />
        {label}
      </MaterialButton>
    );
  }
}

Button.propTypes = {
  label: PropTypes.string.isRequired,
  type: PropTypes.oneOf([ 'button', 'submit' ]),
};

Button.defaultProps = {
  Icon: PlayArrow,
  type: 'button',
};

export default Button;
