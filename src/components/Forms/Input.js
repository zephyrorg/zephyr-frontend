import React from 'react';
import { withStyles } from '@material-ui/core/styles';
import styles from './Input.styles';
import { camelize } from '../../utils/helpers';
import PropTypes from 'prop-types';

@withStyles(styles)
class Input extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      touched: false,
      valid: true,
      validationMessage: null,
    };
  }

  handleFocus = (e) => {
    this.setState({
      touched: true,
    });
  };

  handleChange = (e) => {
    const { onChange, name, validate } = this.props;
    const { value } = e.target;
    this.setState(() => {
      const result = validate(value);
      return {
        valid: result === null,
        validationMessage: result,
      };
    });
    onChange(name)(e);
  };

  render() {
    const { classes, type, label, placeholder, description, name } = this.props;
    const { validationMessage, valid, touched } = this.state;

    const id = name || camelize(label);

    return (
      <div className={classes.root}>
        <label htmlFor={id} className={classes.label}>{label}</label>
        <input
          type={type}
          className={classes.formControl}
          id={id}
          placeholder={placeholder}
          onChange={this.handleChange}
          onFocus={this.handleFocus}
        />
        {validationMessage && !valid && touched ? (
          <small id={`${id}_help`} className={classes.invalid}>{validationMessage}</small>
        ) : null}
        {description ? (
          <small id={`${id}_help`} className={classes.help}>{description}</small>
        ) : null}
      </div>
    );
  }
}

Input.propTypes = {
  type: PropTypes.oneOf([
    'email',
    'text',
    'password',
  ]),
  label: PropTypes.string.isRequired,
  placeholder: PropTypes.string,
  description: PropTypes.string,
  name: PropTypes.string.isRequired,
  onChange: PropTypes.func.isRequired,
  validate: PropTypes.func,
};

Input.defaultProps = {
  type: 'text',
  placeholder: undefined,
  description: undefined,
  validate: () => null,
};

export default Input;
