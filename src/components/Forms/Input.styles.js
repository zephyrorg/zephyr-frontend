const styles = theme => ({
  root: {
    composes: [ 'form-group' ],
    margin: theme.spacing.unit,
  },
  label: {
    fontSize: 16,
    paddingLeft: theme.spacing.unit,
    marginBottom: 0,
  },
  formControl: {
    composes: [ 'form-control' ],
    backgroundColor: 'rgba(49,49,49,1)',
    border: 'none',
    fontFamily: '"IBM Plex Sans", sans-serif',
    color: '#fff !important',
    '&:focus' : {
      outline: 'none',
      backgroundColor: 'rgba(49,49,49,1)',
      border: 'none',
      boxShadow: 'none',
    },
  },
  help: {
    composes: [ 'form-text', 'text-muted' ],
  },
  invalid: {
    composes: [ 'form-text', 'text-danger' ],
  },
});

export default styles;
