const styles = theme => ({
  root: {
    margin: theme.spacing.unit * 2,
  },
  extendedIcon: {
    marginRight: theme.spacing.unit,
  },
});

export default styles;
