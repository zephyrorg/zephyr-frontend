import React from 'react';
import { withStyles } from '@material-ui/core/styles';
import styles from './Select.styles';
import { camelize } from '../../utils/helpers';
import PropTypes from 'prop-types';

@withStyles(styles)
class Select extends React.Component {


  render() {
    const { classes, options, defaultOption, label, name, onChange } = this.props;

    const id = name || camelize(label);

    const finalOptions = [
      defaultOption || null,
      ...options.filter(option => option !== defaultOption),
    ];

    return (
      <div className={classes.root}>
        <label className={classes.label} htmlFor={id}>{label}</label>
        <select onChange={onChange(name)} className={classes.formControl} id={id}>
          {finalOptions.map((option, key) => (
            <option key={key}>{option}</option>
          ))}
        </select>
      </div>
    );
  }
}

Select.propTypes = {
  options: PropTypes.array.isRequired,
  defaultOption: PropTypes.string,
  label: PropTypes.string.isRequired,
  name: PropTypes.string,
};

Select.defaultProps = {
  defaultOptions: undefined,
  name: undefined,
};

export default Select;
