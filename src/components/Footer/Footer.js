import React from 'react';
import { withRouter } from 'react-router-dom';
import { withStyles } from '@material-ui/core/styles';
import styles from './Footer.styles';
import { openNavDrawer } from '../NavDrawer/NavDrawer.actions';
import { connect } from 'react-redux';
import Grid from '@material-ui/core/Grid';
import { Link } from 'react-router-dom';
import classnames from 'classnames';

@withRouter
@withStyles(styles)
@connect(state => ({
  open: state.mobileDrawerState.isOpen,
}), { openNavDrawer })
class Footer extends React.Component {
  render() {
    const { classes, className } = this.props;
    return (
      <div className={classnames(classes.root, className)}>
        <div className={classes.container}>
          <Grid container>
            <Grid item xs={6} className={classes.left}>
            </Grid>
            <Grid item xs={6} className={classes.right}>
              <Link className={classes.link} to="/legal/disclaimer">
                Disclaimer
              </Link>
              <Link className={classes.link} to="/legal/cookies">
                Cookie Policy
              </Link>
            </Grid>
          </Grid>
        </div>
      </div>
    )
  };
}

export default Footer;
