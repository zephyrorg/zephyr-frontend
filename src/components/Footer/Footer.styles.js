const styles = theme => ({
  root: {
    composes: [ 'container-fluid' ],
    padding: 20,
    backgroundColor: '#101010',
    color: '#fff',
  },
  container: {
    composes: [ 'container' ],
  },
  left: {
    textAlign: 'left',
  },
  right: {
    textAlign: 'right',
  },
  link: {
    padding: theme.spacing.unit,
    color: '#fff',
    '&:hover': {
      color: theme.palette.link.hover,
      textDecoration: 'none',
    },
  },
});

export default styles;
