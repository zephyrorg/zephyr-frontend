import React from 'react';
import { connect } from 'react-redux';
import { withRouter } from 'react-router-dom';
import { withStyles } from '@material-ui/core/styles';
import styles from './TopAppBar.styles';
import IconButton from '@material-ui/core/IconButton';
import ExitIcon from '@material-ui/icons/ExitToApp';
import PersonIcon from '@material-ui/icons/Person';
import Button from '@material-ui/core/Button';
// import logo from '../../images/logos/light-flat-logo.png';
import { Link, NavLink } from 'react-router-dom';
import NavDrawer from '../NavDrawer';
import { openNavDrawer } from '../NavDrawer/NavDrawer.actions';
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';
import { logoutUser } from '../../utils/api.actions';
import Tooltip from '@material-ui/core/Tooltip';
import { withCookies } from 'react-cookie';
import HamburgerIcon from '../HamburgerIcon';
import Logo from '../../images/svg/Logo';
import classnames from 'classnames';

@withRouter
@withStyles(styles)
@withCookies
@connect(state => ({
  username: state.apiState.username,
  open: state.mobileDrawerState.isOpen,
}), { openNavDrawer, logoutUser })
class TopAppBar extends React.Component {
  constructor(props) {
    super(props);
    this.items = 0;
    this.state = {
      scrolled: false,
    };
    const self = this;
    window.addEventListener('scroll', function () {
      if (this.scrollY !== 0 && this.scrollY) {
        self.setState({ scrolled: true });
      } else {
        self.setState({ scrolled: false });
      }
    });
  }

  menuIconButton = () => {
    const { classes, openNavDrawer } = this.props;
    return (
        <HamburgerIcon
          open={this.props.open}
          onClick={openNavDrawer}
          className={classes.menuButton}
        />
    );
  };

  menuItem = (label, to, loggedInState = null) => {
    const { loggedIn, classes } = this.props;
    if (loggedInState === true && !loggedIn) return null;
    this.items += 1;

    return (
      <li key={this.items} className={classes.navitem}>
        <NavLink
          exact
          activeClassName={classes.active}
          className={classes.navlink}
          to={to}>
          {label}
        </NavLink>
      </li>
    );
  };

  authNav = () => {
    const { classes, username } = this.props;
    if (username) {
      const name = username.split('#')[0];
      const discriminator = username.split('#')[1];
      return [
        (
          <li key={0} className={classes.navitem}>
            <NavLink
              exact
              className={classes.navlink}
              to="/profile">
              <span>Hi, <span className={classes.name}>{name}</span><sup className={classes.discriminator}>#{discriminator}</sup></span>
            </NavLink>
          </li>
        ),
        (
          <li key={1} className={classes.navitem}>
            <Tooltip
              title="Profile"
              className={classes.iconToolTip}
              classes={{
                popper: classes.tooltip,
              }}
            >
              <NavLink exact activeClassName={classes.activeIcon} className={classes.iconNavLink} to="/profile">
                <IconButton
                  color="inherit"
                  aria-label="Profile"
                  className={classes.iconButton}
                >
                  <PersonIcon />
                </IconButton>
              </NavLink>
            </Tooltip>
          </li>
        ),
        (
          <li key={2} className={classes.navitem}>
            <Tooltip
              title="Logout"
              className={classes.iconToolTip}
              classes={{
                popper: classes.nonavtooltip,
              }}
            >
              <IconButton
                color="inherit"
                aria-label="Logout"
                onClick={this.props.logoutUser}
                className={classes.iconButton}
              >
                <ExitIcon />
              </IconButton>
            </Tooltip>
          </li>
        )
      ];
    }
    return (
      <li className={classes.navitem}>
        <a href={`${process.env.REACT_APP_API_ENDPOINT}/discord/oauth`} className={classes.buttonLink}>
          <Button classes={{
            root: classes.navButton
          }}>
            <FontAwesomeIcon icon={['fab', 'discord']} className={classes.navButtonIcon} /> Login via Discord
          </Button>
        </a>
      </li>
    );
  };

  render() {
    const { classes, username } = this.props;
    const { scrolled } = this.state;
    return (
      <div className={classes.root}>
        <nav className={classnames(classes.nav, {
          [classes.navScrolled]: scrolled,
        })}>
          <div className={classes.navContainer}>
            <div className={classes.mobileBrandContainer}>
              <Link className={classes.brand} to="/">
                <Logo mobile />
                {/*<img alt="Zephyr" src={logo} className={classes.logo} />*/}
              </Link>
            </div>
            <div className={classes.brandContainer}>
              <Link className={classes.brand} to="/">
                <Logo />
                {/*<img alt="Zephyr" src={logo} className={classes.logo} />*/}
              </Link>
            </div>
            {this.menuIconButton()}
            <div className={classes.collapse} id="navbarText">
              <ul className={classes.navbar}>
                {this.menuItem('Home', '/')}
                {this.menuItem('Charter', '/charter')}
                {this.menuItem('Schedule', '/schedule')}
              </ul>
              <ul className={classes.rightNavbar}>
                {this.authNav()}
              </ul>
            </div>
          </div>
        </nav>
        <NavDrawer loggedIn={Boolean(username)} />
      </div>
    );
  }
}

export default TopAppBar;
