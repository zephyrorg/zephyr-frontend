const styles = theme => ({
  root: {
    composes: [ 'container' ],
    padding: 0,
  },
  nav: {
    composes: [
      'navbar',
      'navbar-expand-lg',
      'navbar-dark',
      'fixed-top',
    ],
    backgroundColor: '#101010',
    fontSize: 20,
    height: 80,
    transition: 'all 0.5s',
  },
  navScrolled: {
    '-webkit-filter': 'drop-shadow( -5px -5px 5px rgba(10,10,10,1) ) drop-shadow( 5px 5px 5px rgba(10,10,10,1) )',
    filter: 'drop-shadow( -5px -5px 5px rgba(10,10,10,1) ) drop-shadow( 5px 5px 5px rgba(10,10,10,1) )',
  },
  navContainer: {
    composes: [
      'container',
    ],
  },
  navbar: {
    composes: [
      'navbar-nav',
      'mr-auto',
      'text-center',
    ],
  },
  rightNavbar: {
    composes: [
      'navbar-nav',
      'ml-auto',
      'text-center',
    ],
  },
  navitem: {
    composes: [
      'nav-item',
    ],
    marginLeft: 10,
    marginRight: 10,
    color: '#fff',
  },
  iconButton: {
    ...theme.mixins.noSelect(),
  },
  textitem: {},
  navlink: {
    composes: [
      'nav-link',
    ],
    color: '#fff !important',
    '&:hover': {
      color: '#6ab3df !important',
      cursor: 'pointer',
    },
    transition: 'color 300ms linear',
    webkitTransition: 'color 300ms linear',
    MozTransition: 'color 300ms linear',
    MsTransition: 'color 300ms linear',
    fontWeight: 400,
    willChange: 'color',
  },
  navButton: {
    backgroundColor: '#7289da',
    '&:hover': {
      backgroundColor: '#495991',
    },
    ...theme.mixins.noSelect(),
  },
  navButtonIcon: {
    marginRight: 10,
    fontSize: 22,
  },
  buttonLink: {
    '&:hover': {
      textDecoration: 'none',
    },
  },
  logo: {
    maxHeight: 60,
  },
  collapse: {
    composes: [
      'collapse',
      'navbar-collapse',
    ],
    zIndex: 1010,
  },
  active: {
    color: '#6ab3df !important',
  },
  brand: {
    composes: [
      'navbar-brand',
    ],
    zIndex: 1200,
    margin: 0,
  },
  brandContainer: {
    composes: [
      'mx-auto',
      'order-0',
    ],
    [theme.breakpoints.between('xs', 'md')]: {
      display: 'none',
    },
    [theme.breakpoints.between('md', 'xl')]: {
      display: 'block',
    },
    position: 'absolute',
    left: 'calc(50% - 30px)',
    textAlign: 'center',
    margin:'0 auto',
    zIndex: 1200,
  },
  mobileBrandContainer: {
    [theme.breakpoints.between('xs', 'md')]: {
      display: 'block',
    },
    [theme.breakpoints.between('md', 'xl')]: {
      display: 'none',
    },
  },
  menuButton: {
    color: '#fff',
    [theme.breakpoints.between('md', 'xl')]: {
      display: 'none',
    },
    ...theme.mixins.noSelect(),
  },
  menuAfter: {
    [theme.breakpoints.between('xs', 'sm')]: {
      marginRight: -12,
      display: 'block',
    },
    [theme.breakpoints.between('sm', 'md')]: {
      marginRight: -12,
      display: 'block',
    },
    [theme.breakpoints.between('md', 'xl')]: {
      margin: 0,
      display: 'none',
    },
  },
  menuBefore: {
    [theme.breakpoints.between('xs', 'sm')]: {
      margin: 0,
      display: 'none',
    },
    [theme.breakpoints.between('sm', 'md')]: {
      display: 'none',
      margin: 0,
    },
    [theme.breakpoints.between('md', 'xl')]: {
      display: 'block',
      marginLeft: -12,
      marginRight: 20,
    },
  },
  name: {},
  discriminator: {
    fontSize: 12,
  },
  iconNavLink: {
    color: '#fff',
    '&:hover': {
      color: '#fff !important',
    },
  },
  activeIcon: {
    color: '#6ab3df !important',
  },
  iconToolTip: {
    color: '#fff',
    '&:hover': {
      color: '#fff !important',
    },
  },
  tooltip: {
    marginTop: 10,
    top: 10,
    left: 9,
  },
  nonavtooltip: {
    marginTop: 0,
    top: 10,
    left: 9,
  },
});

export default styles;
