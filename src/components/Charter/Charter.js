import React from 'react';
import styles from './Charter.styles';
import { withStyles } from '@material-ui/core/styles';
import Button from '@material-ui/core/Button';
import { Element, Link } from 'react-scroll';
import Grid from '@material-ui/core/Grid';

const button = (title, section, classes) => (
  <Link to={section} smooth={true} offset={-150} duration={500}>
    <Button
      variant="contained"
      className={classes.button}
      color="default">
      {title}
    </Button>
  </Link>
);

const Charter = (props) => {
  const { classes } = props;
  return (
    <Grid container className={classes.root}>
      <Grid item xs={12} className={classes.header}>
        <h1 className={classes.title}>Legion Charter</h1>
        <div className={classes.links}>
          {button ('Invitation Policy', 'invitationpolicy', classes)}
          {button ('Instance and PvP Grouping/Statics Policy', 'groupingpolicy', classes)}
          {button ('Anti-Cheating Policy', 'anticheatingpolicy', classes)}
          {button ('Discord Policy', 'discordpolicy', classes)}
        </div>
      </Grid>
      <Grid item xs={12} className={classes.paragraph}>
        <p>
          Members of Zephyr will be expected to follow these rules at all times. Violators of Legion rules will be
          confronted by a Senior Officer of the Legion (Brigade General and/or Deputies) and dismissed from the Legion
          if they repeatedly show an inability to comply.
        </p>
      </Grid>
      <Grid item xs={12} className={classes.paragraphList}>
        <ol className={classes.orderedList}>
          <li>
            <p>
              Zephyr members are expected to treat anyone you encounter with respect. This applies to your
              Legion-mates, members of other Legions, and GMs. This also includes a certain level of respect to your
              enemies. We are all Aion players and you are a representative of Zephyr in everything you do.
              Fostering a trustworthy and respectable community within our home, the Legion, will excel us forward in
              our relationships and throughout the game.
            </p>
          </li>
          <li>
            <p>
              Zero Drama Policy. Avoid drama, and keep your emotions in check. By keeping a calm exterior, you
              give the Legion and your Legion-mates a stronger image. When you lose your temper in public, you
              make us all look weak. If you have a problem, take it to whispers, or talk to an Officer of the Legion
              (BG, Deputies, or Centurions).
            </p>
          </li>
          <li>
            <p>
              Any discussions that take place on Zephyr Discord, guild chat, or other Zephyr communication
              mediums are considered confidential and therefore should not be disclosed elsewhere.
            </p>
          </li>
          <li>
            <p>
              In any Legion Event, such as raids, please do what is asked of you. While you may speak up if you
              have a simple suggestion or question, please do not argue with orders in the middle of an event. If you
              disagree with the way an encounter was handled, it can be discussed in whispers or in private on
              Discord afterward. We want to be diligent in our progression through content. Though, we will not forget
              that we are a team, and will therefore act and move like one.
            </p>
          </li>
          <li>
            <p>
              Zephyr is a PvPvE Legion, so please do your best to consistently learn and improve to raise your
              capacity. We all make mistakes, and there are things we could all do better. Strive to polish all such
              aspects of your performance, and to always want to do even better. If you have any questions at all,
              ask your Legion-mates; someone is sure to know how to help. Again, remember you represent the guild
              when you play, and you should always seek to make us look strong.
            </p>
          </li>
          <li>
            <p>
              When a major issue occurs within the Legion, action may be taken immediately. The action taken may
              include, and not be limited to, kicking from in-game Legion and/or removal from the Discord server. The
              reason for this is to avoid any type of speculation and instead influence a productive and mature
              discussion among the Legion regarding the issue. We also want to make the Legion aware that the issue is
              being handled. Upon the post-action investigation's resolution, an official decision will be made and
              detailed to the Legion.
            </p>
          </li>
          <li>
            <p>
              If you have any concerns, please, please bring it to the attention of an Officer of the Legion. Send a
              whisper in-between events, send an in-game mail, or communicate to the Officers via Discord. We want
              to make sure that everyone's concerns are addressed, but we can not solve the issues of which we are
              not aware. If you have a legitimate problem with another Legion member, contact an Officer of the
              Legion in private, and we will do all we can to resolve this while completely respecting your privacy.
            </p>
          </li>
        </ol>
      </Grid>
      <Grid item xs={12} className={classes.paragraphList}>
        <Element name="invitationpolicy">
            <h2>Membership Policy</h2>
        </Element>
        <ol className={classes.orderedList}>
          <li>
            <p>
              We run a fairly tight ship.  We want everyone to be able to play together and have fun, although we must
              protect and foster the community we are building. You must receive a referral from a Centurion or higher
              in order to obtain an invite to the Legion. Legion Members may request someone to be invited by asking a
              Centurion or higher.
            </p>
          </li>
          <li>
            <p>
              Characters that have been offline for more than 14 days are subject to removal from the Legion. Your
              discord role will be demoted to "Friends". With that, you will be able to let us know when you return
              to the game. To avoid being removed from Legion for going on a hiatus, please post in the #a-f-away
              channel in Discord letting us know you will be gone for more than the 14 days.
            </p>
          </li>
        </ol>
      </Grid>
      <Grid item xs={12} sm={12} md={6} className={classes.paragraphListHalfLeft}>
        <Element name="groupingpolicy">
          <h2>Instance and PvP Grouping/Statics Policy</h2>
        </Element>
        <p>
          Legion members are encouraged to create statics within the legion and accept other legion mates as
          substitutes. Of course PUGing is allowed. If you are building a static and find competent players that are
          outside of the legion, you may recruit them. Refer to the Invitation Policy Section for information on
          recruitment requirements and procedure.
        </p>
      </Grid>
      <Grid item xs={12} sm={12} md={6} className={classes.paragraphListHalfRight}>
        <Element name="anticheatingpolicy">
            <h2>Anti-Cheating Policy</h2>
        </Element>
        <p>
          Our Legion promotes a cheat-free environment and we intended to keep the Legion running this way. The Legion
          is extremely strict on the idea of no cheating, and no hacking. Anything that goes against the Terms of
          Service is frowned upon by Zephyr and will result in immediate dismissal from the Legion. We want our foes to
          know that we have beaten them with skill and tactics rather than the use of third-party programs.
        </p>
      </Grid>
      <Grid item xs={12} className={classes.paragraphList}>
        <Element name="discordpolicy">
          <h2>Discord Policy</h2>
        </Element>
        <ol className={classes.orderedList}>
          <li>
            <p>
              During Legion Events, do not have side discussions in the Voice or Text Channels. We need everyone to be
              focused on the task-at-hand.
            </p>
          </li>
          <li>
            <p>
              The best times to speak up are:
            </p>
            <ul>
              <li>
                <p>
                  You notice a large group of enemy players flanking or otherwise sneaking up on the alliance.
                </p>
              </li>
              <li>
                <p>
                  You are in desperate need of heals because you are being focus fired or you need dispells and have yet
                  to receive them.
                </p>
              </li>
              <li>
                <p>
                  You are a scouter for raids.
                </p>
              </li>
              <li>
                <p>
                  Emergency. Please use discretion. If something serious is happening outside of the game or you
                  absolutely must log, please let us know!
                </p>
              </li>
              <li>
                <p>
                  Computer crash or serious lag issues can also be reported if you are playing an integral part in the
                  alliance; such as a tank, healer, or leader.
                </p>
              </li>
            </ul>
          </li>
          <li>
            Do not leave open mic if you are not actively talking in the channel. It is can be aggravating to others and
            unproductive for everyone to hear things unrelated to the context of the voice chat.
          </li>
        </ol>
        <p>
          If you do not follow these rules you will be Globally muted in Discord. Repeat offenders will be preemptively
          muted in Discord during future raids and events. Please use common sense as we cannot account for all possible
          scenarios.
        </p>
      </Grid>
    </Grid>
  );
};

export default withStyles(styles)(Charter);
