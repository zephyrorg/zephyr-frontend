import React from 'react';
import Button from '@material-ui/core/Button';
import Snackbar from '@material-ui/core/Snackbar';
import IconButton from '@material-ui/core/IconButton';
import CloseIcon from '@material-ui/icons/Close';
import { connect } from 'react-redux';
import { closeNotification } from './Notification.actions';

@connect(state => ({
  open: state.notificationState.open,
  content: state.notificationState.content,
  actions: state.notificationState.action,
  verticalAnchor: state.notificationState.verticalAnchor,
  horizontalAnchor: state.notificationState.horizontalAnchor,
}), { closeNotification })
class Notification extends React.Component {
  handleClose = () => {
    const { closeNotification } = this.props;
    closeNotification();
  };

  action = () => {
    const { action } = this.props;
    if (action === null) return null;
    return (
      <Button key={action.label} color="secondary" size="small" onClick={action.onClick}>
        action.label
      </Button>
    );
  };

  render() {
    const { content, open, verticalAnchor, horizontalAnchor } = this.props;
    return (
      <Snackbar
        anchorOrigin={{
          vertical: verticalAnchor,
          horizontal: horizontalAnchor,
        }}
        open={open}
        autoHideDuration={6000}
        onClose={this.handleClose}
        ContentProps={{
          'aria-describedby': 'message-id',
        }}
        message={<span id="message-id">{content}</span>}
        action={[
          this.action(),
          <IconButton
            key="close"
            aria-label="Close"
            color="inherit"
            className={classes.close}
            onClick={this.handleClose}
          >
            <CloseIcon />
          </IconButton>,
        ]}
      />
    )
  }
}

export default Notification;
