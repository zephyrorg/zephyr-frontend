import React from 'react';
import { withStyles } from '@material-ui/core/styles';
import styles from './CharterPage.styles';
import Charter from '../components/CharterNew';

@withStyles(styles)
class CharterPage extends React.Component {
  render() {
    const { classes } = this.props;
    return (
      <div className={classes.root}>
        <Charter />
      </div>
    );
  }
}

export default CharterPage;
