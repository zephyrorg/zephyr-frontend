import React from 'react';
import { withStyles } from '@material-ui/core/styles';
import styles from './AboutPage.styles';
import Logo from '../images/svg/Logo';
import classnames from 'classnames';


@withStyles(styles)
class AboutPage extends React.Component {
  render() {
    const { classes } = this.props;
    return (
      <div>
        <div className={classnames(classes.panel, classes.mission)}>
          <div className={classes.logoDiv}>
            <Logo className={classes.logo} />
          </div>
          <h2>
            Our mission is to bring together a community gamers to work towards the common goal of consistently
            improving ourselves in all aspects of what we do.
          </h2>
          <hr />
          <h4>
            Zephyr is a Gaming Community that seeks to foster a home base for gamers from all different genre and
            titles. We support organizations of any size -- from groups of friends to top tier guilds -- utilizing our
            collective resources and expanding their reaches to attain goals faster, together.
          </h4>
        </div>
      </div>
    );
  }
}

export default AboutPage;
