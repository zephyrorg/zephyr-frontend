import React from 'react';
import { withStyles } from '@material-ui/core/styles';
import styles from './ToolsDropdown.styles';
import { withRouter } from 'react-router-dom';

@withRouter
@withStyles(styles)
class ToolsDropdown extends React.Component {
  goTo = (path) => () => {
    const { history, close } = this.props;
    history.push(path);
    close();
  };

  render() {
    const { classes } = this.props;
    return (
      <ul className={classes.root}>
        <li>
          <span onClick={this.goTo('/aion/tools/translator')}>Translator</span>
        </li>
      </ul>
    );
  }
}

export default ToolsDropdown;
