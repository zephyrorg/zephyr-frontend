import React from 'react';
import { withStyles } from '@material-ui/core/styles';
import styles from './LogoPollAlert.styles';
import ErrorOutlineIcon from '@material-ui/icons/ErrorOutline';
import CloseIcon from '@material-ui/icons/Close';
import * as typeFormEmbed from '@typeform/embed';
import { connect } from 'react-redux';
import { withCookies } from 'react-cookie';
import classnames from 'classnames';
import moment from 'moment';

const TYPEFORM_URL = username => `https://vinlock1.typeform.com/to/COmyrB?id=${username}`;

@withStyles(styles)
@withCookies
@connect(state => ({
  authenticated: state.apiState.authenticated,
  username: state.apiState.username,
}))
class LogoPollAlert extends React.Component {
  constructor(props) {
    super(props);
    const { cookies } = props;
    this.state = {
      visible: props.authenticated && !Boolean(cookies.get('zpoll')),
    };
  }

  hide = () => {
    this.setState({
      visible: false,
    });
  };

  onSubmit = () => {
    const { cookies } = this.props;
    const expires = moment().add(1, 'M').toDate();
    cookies.set('zpoll', true, {
      path: '/',
      expires,
    });
    this.setState({
      visible: false,
    });
  };

  vote = () => {
    const { authenticated, username: discordName } = this.props;
    let username = null;
    if (authenticated) {
      username = discordName.split('#')[0];
      const url = TYPEFORM_URL(username);
      const options = {
        mode: 'drawer_right',
        hideScrollbars: true,
        onSubmit: this.onSubmit,
        autoClose: 5000,
      };
      const popup = typeFormEmbed.makePopup(url, options);
      popup.open();
    }
  };

  render() {
    const { classes, authenticated } = this.props;
    const { visible } = this.state;
    if (authenticated) {
      return (
        <div className={classnames(classes.root, {
          [classes.hideAlert]: !visible,
        })} role="alert">
          <ErrorOutlineIcon/> Vote on Zephyr Gaming's New Logo! <span className={classes.alertLink} onClick={this.vote}>Click Here!</span>
          <button type="button" className="close" onClick={this.hide}>
            <CloseIcon/>
          </button>
        </div>
      );
    }
    return null;
  }
}

export default LogoPollAlert;
