const styles = theme => ({
  root: {
    composes: [
      'alert',
      'alert-dark'
    ],
    borderRadius: 12,
    marginBottom: 0,
  },
  button: {
    margin: theme.spacing.unit,
  },
  alertLink: {
    composes: [
      'alert-link',
    ],
    '&:hover': {
      textDecoration: 'underline',
      cursor: 'pointer',
    },
  },
  hideAlert: {
    display: 'none',
  },
});

export default styles;
